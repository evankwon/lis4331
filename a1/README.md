> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS 4331 

Evan Kwon 

Assignment 1 Requirements:



1. Complete Installs
2. Complete Bitbucket repo
3. Complete hello world java
4. Complete first application
5. Complete contacts application 

README.md file should include the following items:

- Git command definitions
- First Application pictures
- Contacts Application pictures
- Installation complete pictures
- complete contacts application


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git-init, creates an empty git repository 
2. git-status, displays working tree status 
3. git-add, add files content into index
4. git-push, update remote refs and associated content
5. git-commit, saves changes in the repository
6. git-pull retrieves from another repository or branch and integrates
7. git-diff, displays changes between commits and working tree, or others

#### Assignment Screenshots:

*Screenshot of git Installation:

![AMPPS Installation Screenshot](img/git-install-ss.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/ss-hello-world.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/ss-myfirstapp.png)
![Android Studio Installation Screenshot](img/ss-running.png)

*Screenshot of Android Studio - Contacts App*:
![Android Studio Contacts Screenshot](img/ss-mainscreen.png)
![Android Studio Contacts Screenshot](img/ss-info.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")