> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

Advanced Mobile Web App Development 

Evan Kwon

Project 1 Requirements:

My Music Android App

* Splash Screen Shot
* Home Screen
* Paused Song Screen
* Playing Screen




My Music App:

Splash Screen

![AMPPS Installation Screenshot](img/splashscreen.png)

Home Screen:

![JDK Installation Screenshot](img/homescreen.png)

*Pause Screen*:

![Android Studio Installation Screenshot](img/pausescreen.png)

*Playing Screen*:

![Android Studio Installation Screenshot](img/playingscreen.png)



