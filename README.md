## Evan Kwon 

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Java
    - Install Android Studio
    - Create My First App
    - Create Contacts App
    - Provide Screenshots of Installations
    - Create Bitbucket Repo
    - Complete Bitbucket Tutorial
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Research background color/theme 
    - Create Launcher Icon
    - Create Tip Calculator App
    - Screenshots of Applications and Skillsets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Splash Screen SS
    - Unpopulated UI SS
    - Toast Notification SS
    - Converted Currency SS

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - My Music App
    - Skill Set 7-9


5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Screenshot of running Mortgage Rate App's unpopulated user interface
    - Screenshot of running Mortgage Rate App's Splash Screen
    - Screenshot of running Mortgage Rate App's invalid entry
    - Screenshot of running Mortgage Rate App's valid entry
    - SS 10-12

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Screenshot of News Reader App Items Activity Screen
    - Screenshot of News Reader App Item Activity Screen
    - Screenshot of News Reader App Read More Screen

6. [P2 README.md](p2/README.md "My P2 README.md file")
    - Screenshot of Splash Screen
    - Screenshot of Add User
    - Screenshot of Update User
    - Screenshot of Delete



