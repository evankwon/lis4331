

LIS 4331

## Evan Kwon 

### Assignment 2 Requirements:

*Research drop-down menus in Android studio
*Add background color/theme
*Create launcher icon
*Create Tip Calculator App



#### Assignment Screenshots:

*Screenshot of Unpopulated User Interface

![Unpopulated Screenshot](img/unpopulated.png)

*Screenshot of Populated User Interface

![Populated Screenshot](img/populated.png)

*Screenshot of SS1

![SS1 Screenshot](img/ss1.png)

*Screenshot of SS2

![SS2 Screenshot](img/ss2.png)

*Screenshot of SS3

![SS3 Screenshot](img/ss3.png)





