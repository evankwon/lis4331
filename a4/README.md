
# LIS 4331 Advanced Mobile Web Application Development 
## Evan Kwon

### Assignment 4 Requirements:


1. Create Mortgage Rate Application
2. Screenshot of running Mortgage Rate App's Splash Screen
3. Screenshot of running Mortgage Rate App's unpopulated user interface
4. Screenshot of running Mortgage Rate App's invalid entry
5. Screenshot of running Mortgage Rate App's valid entry
6. Screenshot of skillsets

#### README.md file should include the following items:

* Main Screen SS
* Splash SS
* Valid Entry SS
* Invalid Entry SS


#### Assignment Screenshots:
*Screenshot Main Screen:

![AMPPS Installation Screenshot](img/main.png)

*Screenshot Splash Screen:

![AMPPS Installation Screenshot](img/splash.png)

*Screenshot of Valid Entry*:

![JDK Installation Screenshot](img/valid.png)

*Screenshot of Invalid Entry*:

![Android Studio Installation Screenshot](img/invalid.png)






