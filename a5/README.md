
# LIS 4331 Advanced Mobile Web Application Development 
## Evan Kwon

### Assignment 5 Requirements:


1. Screenshot of News Reader App Items Activity Screen
2. Screenshot of News Reader App Item Activity Screen
3. Screenshot of News Reader App Read More Screen


#### Assignment Screenshots:
*Screenshot of News Reader App Items Activity Screen

![AMPPS Installation Screenshot](img/items.png)

*Screenshot of News Reader App Item Activity Screen

![AMPPS Installation Screenshot](img/item.png)

*Screenshot of News Reader App Read More Screen

![JDK Installation Screenshot](img/readmore.png)


