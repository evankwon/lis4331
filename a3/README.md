

# LIS 4331 Advanced Mobile Web Application Development 
## Evan Kwon

### Assignment 3 Requirements:


1. Field to enter U.S. dollar amount: 1-100,000
2. Must Have toast notification
3. Must have correct sign for currency
4. Must Add Background Color
5. Have SPLASH scree
6. Screenshots of application

#### README.md file should include the following items:

* Splash Screen SS
* Unpopulated UI SS
* Toast notification SS
* Converted Currency SS


#### Assignment Screenshots:

*Screenshot Splash Screen:

![AMPPS Installation Screenshot](img/splashscreen.png)

*Screenshot of Blank UI*:

![JDK Installation Screenshot](img/blank.png)

*Screenshot of Toast Notification*:

![Android Studio Installation Screenshot](img/toast.png)

*Screenshot of Converted Currency:

![Android Studio Installation Screenshot](img/converted.png)


