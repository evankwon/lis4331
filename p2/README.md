> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4431

## Evan Kwon

### Project 2 Requirements:


#### README.md file should include the following items:

* Include splash screen.
* Must use persistent data: SQLite database.
* Insert at least five users.
* Must add background color(s) or theme.
* Create and display launcher icon image.

#### Assignment Screenshots:

*Screenshot of Splash Screen:

![Android Studio Installation Screenshot](img/splash.png)

*Screenshot of Add User*:

![AMPPS Installation Screenshot](img/add.png)

*Screenshot of Update User*:

![JDK Installation Screenshot](img/update.png)

*Screenshot of Delete*:

![Android Studio Installation Screenshot](img/delete.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
